<?php
function categorias(){
    require_once('conexaoBanco.php');
    $sqlCategoria = "SELECT * FROM CATEGORIA";
    $categorias = $con->query($sqlCategoria);
    if ($categorias->num_rows > 0) {
        // existe categoria
        while($categoria = $categorias->fetch_assoc()) {
            echo "<option value='".$categoria["ID"]."'>".$categoria["NOME"]."</option>";
        }
    }
}

function links(){
    require_once('conexaoBanco.php');
    $slqLinks = "SELECT LIN.TITULO AS TITULO, LIN.URL AS URL, CAT.NOME AS CATEGORIA FROM LINK LIN JOIN CATEGORIA CAT ON LIN.ID_CATEGORIA = CAT.ID ORDER BY CAT.ID ";
    $links = $con->query($slqLinks);
    if ($links->num_rows > 0) {
        // existe link
        while($link = $links->fetch_assoc()) {
            $objetoLink[] = $link; 
        }
        $link = json_encode($objetoLink); 
        $link = str_replace("\\","",$link); 
        echo "<input id='link' type='hidden' value=".$link.">";
    }
    
}
?>