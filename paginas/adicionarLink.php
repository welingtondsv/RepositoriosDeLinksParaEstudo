<?php
 require_once('../componentes/titulo.php');
 require_once('../componentes/modalNovaCategoria.php');
 require_once('../PHP/funcoes.php');
?>
<link rel="stylesheet" href="../estilo/cssAdicionarLink.css">
<script src="../script/scriptCriarLink.js"></script>


<div class="container formulario">
  <form action="../PHP/adicionarLink.php" method="post">
  <fieldset class="form-group row">
      <legend class="col-form-legend col-sm-1-12">Formulario inserção de links</legend>
      <div class="col-sm-1-12">
      </div>
    </fieldset>
    <div class="form-group row">
      <div class="col-sm-1-12 col-md-12">
      <label for="link">Insira o link a ser salvo</label>
        <input type="url" class="form-control" name="link" id="link" placeholder="">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-sm-1-12 col-md-6">
      <label for="titulo">Insira o titulo do link</label>
        <input type="text" class="form-control" name="titulo" id="titulo" placeholder="">
      </div>
      <div class="col-sm-1-12 col-md-6">
        <label for="titulo">Categoria</label>
        <select name="categorias" id="categorias" class="form-control" required>
        <option value=''>-----Selecione-------</option>
          <option id='criar' value="nova">Criar nova Categoria</option>
          <?php categorias();?>
        </select>
      </div>
    </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10 col-md-4 dropdown">
        <button type="submit" class="btn btn-primary">Salvar</button>
      </div>
    </div>
  </form>
</div> 
</div>  
</html>
