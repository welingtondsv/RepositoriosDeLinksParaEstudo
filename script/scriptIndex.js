$(function(){
    var link = $.parseJSON($('#link').val());
    var key = '5a1989adbd19be6ceb6cde9578ed6229ba2da69234a14';
    var categoriaCriada = 'primeiraExecução';

    function criaRowCategoria(categoria){
        var linhaCategoria = "";
        linhaCategoria += '<div id="'+ categoria +'" class="row pai-categoria">';
        linhaCategoria += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 categoria" style="margin-left:40%;">';
        linhaCategoria += "<strong> <h2 style='font-size:50px;' class='nomeCategoria'>" +categoria+ "</h2> </strong> <br>";
        linhaCategoria += '</div>';
        linhaCategoria += '</div>';
        return linhaCategoria;
    }

    function criarCard(parametros){
        var linhaLink ='';
        linhaLink +='<div class="row linhaLink">';
        linhaLink += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
        linhaLink += "<h3 class='titulo-link'>"+parametros.tituloUsuario+"</h3>";
        linhaLink +='<div id="card" class="card">';
        linhaLink +='<img class="card-img-top" src="'+parametros.imagemLink+'" alt="SEM IMAGEM">';
        linhaLink +='<div class="card-block">';
        linhaLink +='<a class="link" href="'+parametros.url+'" ><h4 class="card-title">'+parametros.titulo+'</h4></a>';
        linhaLink +='<p class="card-text">'+parametros.descricaoLink+'</p>';
        linhaLink +='</div>';
        linhaLink +='</div>';
        linhaLink +='</div>';
        linhaLink +='</div>';
        return linhaLink;
    }
    for(var i = 0; i<link.length; i++){
        var url = link[i]['URL'];
        var categoria = link[i]['CATEGORIA'];
        var titulo = link[i]['TITULO'];
        requisitarDadosPagina(url, categoria, titulo);
    }

    function requisitarDadosPagina(url, categoria, titulo){
        $.ajax({
            url: "https://api.linkpreview.net?key="+key +"&q="+url,
            success: function(resultado){
                //criação do objeto com os parametos necessarios para a função de criarCard
                var parametros = {};
                parametros.titulo = resultado['title'];
                parametros.descricaoLink = resultado['description'];
                parametros.imagemLink = resultado['image'];
                parametros.url = url;
                parametros.categoria = categoria;
                parametros.tituloUsuario = titulo;
                parametros.categoriaCriada = categoriaCriada;
                //verifica se a linha da categoria ainda não foi criada
                 if((parametros.categoriaCriada == "primeiraExecução")|| categoriaCriada != parametros.categoria){
                    var linhaCategoria = criaRowCategoria(parametros.categoria);
                    $('#containerLinks').append(linhaCategoria);
                    categoriaCriada = parametros.categoria;
                    var linhaLink = criarCard(parametros);
                    $('#'+parametros.categoria).append(linhaLink);
                 }else{
                    var linhaLink = criarCard(parametros);
                    $('#'+parametros.categoria).append(linhaLink);
                 }
            }
        });
    }

    // função de pesquisar

    $("#campoPesquisa").keyup(function(){
        $('.titulo').hide();
        $('.pai-categoria').hide();
        $('.linhaLink').hide();
        var pesquisa = $('#pesquisa').val();
        if(pesquisa == ""){
            $('.titulo').show();
            $('.pai-categoria').each(function(){
                    $(this).show();
                    $(this).children().show();
            });
            $('.titulo-link').each(function(){
                $(this).parent().parent().show();
            });
        }

        // verificar se acha alguma categoria de acordo com a pesquisa
        $('.pai-categoria').each(function(){
            if($(this).text().indexOf(pesquisa) != -1 ){
                $(this).show();
                $(this).children().show();
            }else{
                $(this).hide();
                $(this).children().hide();
                //se não encontrar procura por titulo
                $('.linhaLink').each(function(){
                    if($(this).text().indexOf(pesquisa)  != -1 ){
                        $(this).show();
                    }else{
                        console.log('nãooo');
                        $(this).hide();
                    }
                });
            }
        }); 
    });

});