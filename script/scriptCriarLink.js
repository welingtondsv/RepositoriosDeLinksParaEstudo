$(document).ready(function() {
    $('#criar').click(function(){
        $('#Modal').modal('toggle');
    });

    $('#CriarCategoria').click(function(){
        $('#Modal').modal('toggle');
        if($('#nomeCategoria').val() == ''){
            alert('Nem uma categoria informada');
        }else{
            var repetida = false;
            //verifica se ja existe a categoria
            $('#categorias option').each(function(){
                if($(this).text() == $('#nomeCategoria').val()){
                    alert('Categoria ja existe');
                    $('#nomeCategoria').val('');
                    $("#categorias option:first").prop('selected', true);
                    repetida = true;
                }
            });
            //se não existe então cria
            if(!repetida){
                var novaCategoria = $('#nomeCategoria').val();
                $('#nomeCategoria').val('');
                $('#categorias').append("<option value='"+ novaCategoria +"'>" + novaCategoria + "</option>");
                $('#categorias option[value="'+ novaCategoria+ '"]').prop('selected', true);
            }       
        }
    });
});