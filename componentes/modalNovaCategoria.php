<?php
 require_once('headPagina.php');
?>
<link rel="stylesheet" href="../estilo/estiloModal.css">

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="labelModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="labelModal">Nova Categoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="nomeCategoria" class="col-form-label">Categoria:</label>
            <input type="text" class="form-control" id="nomeCategoria">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button id="CriarCategoria" type="button" class="btn btn-primary">Criar</button>
      </div>
    </div>
  </div>
</div>