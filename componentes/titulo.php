<?php
 require_once('headPagina.php');
?>
<link rel="stylesheet" href="../estilo/titulo.css">
<body>
    <div class="container">
        <div class="row">
            <nav class="nav justify-content-center|justify-content-end">
            <a class="nav-link active" href="../paginas/index.php">Pagina inicial</a>
              <a id="pesquisar" class="nav-link active" href="#" >Pesquisar</a>
              <div id="campoPesquisa" class="row">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <input type="text" id="pesquisa">
                </div>
              </div>
              
              <a class="nav-link" href="adicionarLink.php">Adicionar link</a>
            </nav>
        </div>
        <div class="row titulo">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1>Repositorio de Links</h1>    
            </div>
        </div>
    </div>
</body>